package cn.doitedu.rtmk.demo.demo6;

import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.util.Collector;
import org.roaringbitmap.RoaringBitmap;

import java.io.IOException;

public interface RuleModelCalculator {

    String getRuleId();

    void init(String ruleParamJson, RuntimeContext runtimeContext, RoaringBitmap btm) throws IOException;

    void calc(
            EventBean eventBean,
            Collector<RtmkMessage> out
    ) throws Exception;
}
