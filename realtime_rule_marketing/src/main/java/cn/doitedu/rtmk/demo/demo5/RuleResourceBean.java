package cn.doitedu.rtmk.demo.demo5;

import lombok.*;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class RuleResourceBean {
    int operateType;
    String rule_id;
    String rule_model_id;
    String rule_param_json;
    String rule_param_groovy_code;
    String rule_model_groovy_code;
    int status;
    String creator;
    Timestamp create_time;
    Timestamp update_time;
}
