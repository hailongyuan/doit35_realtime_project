package cn.doitedu.rtmk.demo.demo4;

import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.util.Collector;

import java.io.IOException;

public interface RuleModelCalculator {

    String getRuleId();

    void init(String ruleParamJson, RuntimeContext runtimeContext) throws IOException;

    void calc(
            EventBean eventBean,
            Collector<RtmkMessage> out
    ) throws Exception;
}
