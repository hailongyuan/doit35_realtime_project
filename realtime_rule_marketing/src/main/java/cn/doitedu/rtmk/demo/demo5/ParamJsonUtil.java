package cn.doitedu.rtmk.demo.demo5;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import groovy.lang.GroovyClassLoader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;

import java.io.IOException;
import java.util.Map;

@Slf4j
public class ParamJsonUtil {

    public static RuleModelCalculator generateInitCalculatorByGroovyCode(GroovyClassLoader groovyClassLoader,
                                                                         RuleResourceBean resourceBean, String ruleId,
                                                                         RuntimeContext runtimeContext) throws InstantiationException, IllegalAccessException, IOException {
        String rule_param_json = resourceBean.getRule_param_json();
        String rule_model_groovy_code = resourceBean.getRule_model_groovy_code();
        String rule_param_groovy_code = resourceBean.getRule_param_groovy_code();

        // 加载规则模型对应的参数封装类的groovy代码
        groovyClassLoader.parseClass(rule_param_groovy_code);

        // 根据规则所携带的模型运算机代码，构造出运算机实例对象
        Class aClass = groovyClassLoader.parseClass(rule_model_groovy_code);
        RuleModelCalculator calculator = (RuleModelCalculator) aClass.newInstance();

        // 初始化运算机
        calculator.init(rule_param_json,runtimeContext);

        // 然后将运算机  放入  运算机容器Map
        return calculator;
    }




}
