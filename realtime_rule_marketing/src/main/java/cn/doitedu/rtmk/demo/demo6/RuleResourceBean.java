package cn.doitedu.rtmk.demo.demo6;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.roaringbitmap.RoaringBitmap;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class RuleResourceBean {
    int operateType;
    String rule_id;
    String rule_model_id;
    String rule_param_json;
    String rule_param_groovy_code;
    String rule_model_groovy_code;
    int status;
    String creator;
    RoaringBitmap staticProfileCrowdBitmap;
    Timestamp create_time;
    Timestamp update_time;
}
