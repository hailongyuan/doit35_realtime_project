package cn.doitedu.rtmk.demo.demo3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class RuleModel002ParamBean {

    String ruleModelId;
    String ruleId;

    private String fireEventId;
    // 动态画像条件1的事件id,比如  add_cart
    private String dynamicProfileEventId;
    // 比如  item_price
    private String dynamicProfilePropertyKey;
    private Double dynamicProfilePropertyValue;

}
