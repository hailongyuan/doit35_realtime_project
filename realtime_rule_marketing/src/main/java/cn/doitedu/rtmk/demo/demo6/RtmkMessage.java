package cn.doitedu.rtmk.demo.demo6;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class RtmkMessage {
    int userId;
    long timeStamp;
    String ruleId;
}