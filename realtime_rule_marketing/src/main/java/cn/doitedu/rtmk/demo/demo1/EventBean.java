package cn.doitedu.rtmk.demo.demo1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class EventBean {
    int userId;
    String eventId;
    Map<String, String> properties;
    long timeStamp;
}