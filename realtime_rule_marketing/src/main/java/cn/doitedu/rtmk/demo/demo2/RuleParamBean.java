package cn.doitedu.rtmk.demo.demo2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class RuleParamBean {

    String fireEventId;
    String fileEventPropertyKey;
    int fileEventPropertyValue;

    int ageConditionMin;
    int ageConditionMax;
    String genderCondition;
    int tg03Condition;

    int dynamicConditionValue1;
    int dynamicConditionvalue2;

}
