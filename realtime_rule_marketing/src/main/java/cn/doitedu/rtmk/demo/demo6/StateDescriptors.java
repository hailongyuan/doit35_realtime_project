package cn.doitedu.rtmk.demo.demo6;

import org.apache.flink.api.common.state.MapStateDescriptor;

public class StateDescriptors {

    // 规则资源广播状态描述器
    public static final MapStateDescriptor<String, RuleResourceBean> RULE_RES_STATE_DESC = new MapStateDescriptor<>("param_bc_state", String.class, RuleResourceBean.class);

}
