package cn.doitedu.rtmk.demo.demo2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class RtmkMessage {
    int userId;
    long timeStamp;
    String ruleId;
}