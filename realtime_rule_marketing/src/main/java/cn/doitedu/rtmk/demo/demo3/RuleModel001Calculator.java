package cn.doitedu.rtmk.demo.demo3;

import com.alibaba.fastjson.JSON;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.util.Collector;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

public class RuleModel001Calculator {

    RuleModel001ParamBean ruleParamBean;

    public void init(String ruleParamJson){
        ruleParamBean = JSON.parseObject(ruleParamJson, RuleModel001ParamBean.class);
    }


    public void calc(
                     Demo3.EventBean eventBean,
                     ListState<Demo3.EventBean> con1State,
                     ListState<Demo3.EventBean> con2State,
                     Table table, Collector<Demo3.RtmkMessage> out
                     ) throws Exception {
        // 进行规则所需要的实时画像标签的计算
        // 实时画像条件：  item_share 发生次数>=2 ;  item_like 发生次数>=1
        if (eventBean.getEventId().equals("item_share")) con1State.add(eventBean);
        if (eventBean.getEventId().equals("item_like")) con2State.add(eventBean);

        // 判断是否是预定义的营销规则的触发事件 ：加购事件（加购的商品价格>100)
        if (eventBean.getEventId().equals(ruleParamBean.getFireEventId()) && Double.parseDouble(eventBean.getProperties().get(ruleParamBean.getFileEventPropertyKey())) > ruleParamBean.getFileEventPropertyValue()) {
            System.out.println(eventBean.getUserId() + " 触发了规则 " + ruleParamBean.getRuleId());

            // 判断该事件的行为人是否满足营销规则中定义的判断条件
            // 离线静态画像条件：从离线画像库中可以查询到的条件
            //  age : (20,40]
            //  gender: male
            //  半年内月平均消费额(tg03): > 1000
            Get get = new Get(Bytes.toBytes(eventBean.getUserId()));
            Result result = table.get(get);

            int age = Bytes.toInt(result.getValue(Bytes.toBytes("f"), Bytes.toBytes("age")));
            String gender = Bytes.toString(result.getValue(Bytes.toBytes("f"), Bytes.toBytes("gender")));
            int tg03 = Bytes.toInt(result.getValue(Bytes.toBytes("f"), Bytes.toBytes("tg03")));

            if (age > ruleParamBean.getAgeConditionMin() && age <= ruleParamBean.getAgeConditionMax() && gender.equals(ruleParamBean.getGenderCondition()) && tg03 > ruleParamBean.getTg03Condition()) {
                //  实时动态画像条件：
                //  最近 5分钟发生过 ： 分享行为 2次以上
                //  最近 30分钟发生过 ：点赞行为 1次以上
                int con1Count = 0;
                for (Demo3.EventBean bean : con1State.get()) {
                    con1Count++;
                }
                if (con1Count > ruleParamBean.getDynamicConditionValue1()) {
                    int con2Count = 0;
                    for (Demo3.EventBean bean : con2State.get()) {
                        con2Count++;
                    }
                    if (con2Count > ruleParamBean.getDynamicConditionvalue2()) {
                        // 走到这里，就所有的条件（包含离线静态画像标签条件和 实时动态画像标签条件）都满足了
                        out.collect(new Demo3.RtmkMessage(eventBean.getUserId(), eventBean.getTimeStamp(), ruleParamBean.ruleId));
                    }
                }
            }
        }
    }
}
