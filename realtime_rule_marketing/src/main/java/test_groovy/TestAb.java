package test_groovy;

import groovy.lang.GroovyClassLoader;

public class TestAb {

    public static void main(String[] args) throws InstantiationException, IllegalAccessException {


        /*B b = new B();
        b.speak();*/


        String aCode = "package groovy_test\n" +
                "class A {\n" +
                "    void say(){\n" +
                "        println(\"我要和你嘿嘿嘿\")\n" +
                "    }\n" +
                "}\n";

        String bCode = "package groovy_test\n" +
                "class B implements Binterface {\n" +
                "    void speak(){\n" +
                "        A a  = new A()\n" +
                "        a.say()\n" +
                "    }\n" +
                "}\n";


        GroovyClassLoader loader = new GroovyClassLoader();

        loader.parseClass(aCode);
        Class aClass = loader.parseClass(bCode);

        Binterface b = (Binterface) aClass.newInstance();
        b.speak();


    }
}
