package test_groovy;

public class CodeOffer {

    public static String code = "package groovy_test\n" +
            "\n" +
            "class GroovyHelloWorld implements HelloWorldInterface{\n" +
            "    static void main(String[] args) {\n" +
            "\n" +
            "        println(\"hello world\")\n" +
            "        ArrayList lst = new ArrayList<String>()\n" +
            "        lst.add(\"haha\")\n" +
            "        lst.add(\"xixi\")\n" +
            "\n" +
            "        System.out.println(lst)\n" +
            "    }\n" +
            "\n" +
            "\n" +
            "    void say(){\n" +
            "        println(\"我爱你\")\n" +
            "    }\n" +
            "}\n";
}
