package test_groovy;

import groovy.lang.GroovyClassLoader;

import java.sql.*;

public class JavaHelloWord {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, SQLException {
        /**
         * 如果系统中拥有这个类，可以这样常规调用
         */
        /*GroovyHelloWorld groovyHelloWorld = new GroovyHelloWorld();
        groovyHelloWorld.say();*/

        /**
         * 如果系统中没有这个类，而是从外界得到一个该类的源代码字符串
         * 可以这样调
         */
        /*GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
        Class aClass = groovyClassLoader.parseClass(CodeOffer.code);

        HelloWorldInterface o = (HelloWorldInterface) aClass.newInstance();
        o.say();*/

        /**
         * 从数据库中获取 groovy类源码字符串，来调用
         */
        Connection conn = DriverManager.getConnection("jdbc:mysql://doitedu:3306/rtmk", "root", "root");
        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery("select model_caculator_code from rule_meta");
        result.next();
        String code = result.getString(1);

        GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
        Class aClass = groovyClassLoader.parseClass(code);
        HelloWorldInterface o = (HelloWorldInterface) aClass.newInstance();
        o.say();


    }
}
