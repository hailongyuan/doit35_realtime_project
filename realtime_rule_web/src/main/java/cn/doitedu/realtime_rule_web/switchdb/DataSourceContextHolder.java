package cn.doitedu.realtime_rule_web.switchdb;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataSourceContextHolder {
    // 默认数据源
    public static final String DEFAULT_DS = "master";

    private static final ThreadLocal<String> contextHolder = new ThreadLocal<>();

    public static void setDB(String dbType) {
        log.info("切换至" + dbType + "数据源");
        contextHolder.set(dbType);
    }

    public static String getDB() {
        return (contextHolder.get());
    }

    public static void clearDB() {
        contextHolder.remove();
    }
}