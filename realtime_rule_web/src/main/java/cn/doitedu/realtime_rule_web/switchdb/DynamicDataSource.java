package cn.doitedu.realtime_rule_web.switchdb;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class DynamicDataSource extends AbstractRoutingDataSource {
    @Override protected Object determineCurrentLookupKey() {
        return DataSourceContextHolder.getDB();
    }
}
