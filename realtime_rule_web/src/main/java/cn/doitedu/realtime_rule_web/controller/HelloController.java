package cn.doitedu.realtime_rule_web.controller;

import cn.doitedu.realtime_rule_web.pojo.DorisBean;
import cn.doitedu.realtime_rule_web.pojo.HelloForm;
import cn.doitedu.realtime_rule_web.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class HelloController {

    @Autowired
    HelloService helloService;

    @RequestMapping("/")
    public String index(){
        return "/index.html";
    }

    @RequestMapping("/saveForm")
    @ResponseBody
    public String saveForm(@RequestBody HelloForm helloForm){
        helloService.insertHelloForm(helloForm);
        return "ok";
    }

    @RequestMapping("/getForm")
    @ResponseBody
    public HelloForm getForm(String name){
        System.out.println(name);
        return helloService.getHelloForm(name);
    }

    @RequestMapping("/getDoris")
    @ResponseBody
    public DorisBean getDorisBean(Integer id){
        return helloService.getDorisBean(id);
    }

    @RequestMapping("/getDoris2")
    @ResponseBody
    public List<DorisBean> getDorisBean2(Integer id){
        return helloService.getDorisBean2(id);
    }
}
