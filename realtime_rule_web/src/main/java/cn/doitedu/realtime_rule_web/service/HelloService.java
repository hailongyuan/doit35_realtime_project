package cn.doitedu.realtime_rule_web.service;

import cn.doitedu.realtime_rule_web.dao.HelloDorisMapper;
import cn.doitedu.realtime_rule_web.dao.HelloMysqlMapper;
import cn.doitedu.realtime_rule_web.switchdb.DataBase;
import cn.doitedu.realtime_rule_web.pojo.DorisBean;
import cn.doitedu.realtime_rule_web.pojo.HelloForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HelloService {

    @Autowired
    HelloMysqlMapper helloMysqlMapper;

    @Autowired
    HelloDorisMapper helloDorisMapper;

    @DataBase("mysql")
    public String insertHelloForm(HelloForm helloForm){
        helloMysqlMapper.addHello(helloForm);
        return "1";
    }

    @DataBase("mysql")
    public HelloForm getHelloForm(String name){
        return helloMysqlMapper.findForm(name);
    }


    @DataBase("doris")
    public DorisBean getDorisBean(Integer id) {
        return helloDorisMapper.findDorisBean(id);
    }

    @DataBase("doris")
    public List<DorisBean> getDorisBean2(Integer id) {
        return helloDorisMapper.findDorisBeans(id);
    }
}
