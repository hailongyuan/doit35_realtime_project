package cn.doitedu.realtime_rule_web.dao;

import cn.doitedu.realtime_rule_web.pojo.DorisBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface HelloDorisMapper {

    @Select("select * from user where id = #{id}")
    DorisBean findDorisBean(Integer id);


    @Select("select * from user where id > #{id}")
    List<DorisBean> findDorisBeans(Integer id);

}
