package cn.doitedu.realtime_rule_web.dao;

import cn.doitedu.realtime_rule_web.pojo.DorisBean;
import cn.doitedu.realtime_rule_web.pojo.HelloForm;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface HelloMysqlMapper {
    @Insert("insert into hello_form values (#{name},#{email})")
    void addHello(HelloForm helloForm);

    @Select("select * from hello_form where name = #{name}")
    HelloForm findForm(String name);

}
