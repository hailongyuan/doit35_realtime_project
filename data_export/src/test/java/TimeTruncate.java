import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeTruncate {

    public static void main(String[] args) throws ParseException {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dt = "2022-12-13 16:12:00";

        // 60分切
        Date date = sdf.parse(dt);
        long time = date.getTime();

        long time60 = time / (60*60*1000);
        String date60 = sdf.format(new Date(time60 * 60*60*1000));
        System.out.println(date60);

        // 30分切
        long time30 = time / (30*60*1000);
        String date30 = sdf.format(new Date(time30 * 30*60*1000));
        System.out.println(date30);

        // 10分切
        long time10 = time / (10*60*1000);
        String date10 = sdf.format(new Date(time10 * 10*60*1000));
        System.out.println(date10);




    }
}
