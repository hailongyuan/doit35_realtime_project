import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestTimeTruncate {
    public static void main(String[] args) throws ParseException {

        String time = "2023-02-02 17:24:16";
        System.out.println(time);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = sdf.parse(time);
        long ts = date.getTime();

        long hTrucnt = (ts/(60*60*1000))*(60*60*1000);
        System.out.println(sdf.format(new Date(hTrucnt)));

        long halfHourTrucnt = (ts/(30*60*1000))*(30*60*1000);
        System.out.println(sdf.format(new Date(halfHourTrucnt)));

        long min10Trucnt = (ts/(10*60*1000))*(10*60*1000);
        System.out.println(sdf.format(new Date(min10Trucnt)));


    }
}
