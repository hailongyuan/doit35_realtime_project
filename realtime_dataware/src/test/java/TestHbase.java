import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class TestHbase {
    public static void main(String[] args) throws IOException {
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum","doitedu:2181");

        Connection conn = ConnectionFactory.createConnection(conf);

        Table table = conn.getTable(TableName.valueOf("dim_geo_area"));

        Scan scan = new Scan();
        ResultScanner scanner = table.getScanner(scan);

        List<byte[]> rows = new ArrayList<>();

        for (Result result : scanner) {
            byte[] row = result.getRow();
            rows.add(row);
        }
        Random random = new Random();
        int size = rows.size();
        System.out.println("------" + size );
        byte[] f = "f".getBytes();
        byte[] p = "p".getBytes();

        long start = System.currentTimeMillis();
        System.out.println("start: "+ start);
        for(int i=0;i<300000;i++){

            Result result = table.get(new Get(rows.get(random.nextInt(size))));
            result.getValue(f,p);
        }
        long end = System.currentTimeMillis();
        System.out.println("end: "+end);

        System.out.println("总耗时(s): " + (end-start)/1000);  // 总耗时(s): 180
        System.out.println("平均每次: " + (end-start)/300000f);  // 平均每次: 1.80947ms


        /**
         * 网上专业压测
         *  [READ], AverageLatency(us), 909.1641435940626
         *  [READ], MinLatency(us), 201
         *  [READ], MaxLatency(us), 76031
         *
         *  0.9ms/次 ==>  1000次/s
         */


        table.close();
        conn.close();

    }

}
