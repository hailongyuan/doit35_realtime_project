import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class TestHttpRequest {

    public static void main(String[] args) throws IOException {

        // 构造http客户端
        CloseableHttpClient client = HttpClientBuilder.create().build();

        // 构造post请求
        HttpPost post = new HttpPost("http://doitedu:8081/api/post/simwords");
        // 设置请求头参数
        post.addHeader("Content-type", "application/json; charset=utf-8");
        post.addHeader("Accept", "application/json");
        // 请求体参数
        post.setEntity(new StringEntity("{\"origin\":\"雀巢醇品咖啡\"}", StandardCharsets.UTF_8));

        // 执行请求
        CloseableHttpResponse response = client.execute(post);

        // 获取结果
        HttpEntity responseEntity = response.getEntity();
        System.out.println(EntityUtils.toString(responseEntity, "utf-8"));


    }

}
